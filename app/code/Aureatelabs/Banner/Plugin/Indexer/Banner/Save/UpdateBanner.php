<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Banner
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Banner\Plugin\Indexer\Banner\Save;

use Aureatelabs\Banner\Model\Indexer\BannerProcessor;
use Aureatelabs\Banner\Model\Banner;

/**
 * Class UpdateBanner
 * @package Aureatelabs\Banner\Plugin\Indexer\Banner\Save
 */
class UpdateBanner
{
    /**
     * @var BannerProcessor
     */
    private $bannerProcessor;

    /**
     * Save constructor.
     *
     * @param BannerProcessor $bannerProcessor
     */
    public function __construct(BannerProcessor $bannerProcessor)
    {
        $this->bannerProcessor = $bannerProcessor;
    }

    /**
     * @param Banner $banner
     * @param Banner $result
     *
     * @return Banner
     */
    public function afterAfterSave(Banner $banner, Banner $result)
    {
        $result->getResource()->addCommitCallback(function () use ($banner) {
            $this->bannerProcessor->reindexRow($banner->getId());
        });

        return $result;
    }

    /**
     * @param Banner $banner
     * @param Banner $result
     *
     * @return Banner
     */
    public function afterAfterDeleteCommit(Banner $banner, Banner $result)
    {
        $this->bannerProcessor->reindexRow($banner->getId());

        return $result;
    }
}
